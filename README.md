# challenge



## Requirement 
* terraform 1.1.4
* terragrunt v0.35.6
* aws profile

## Getting Started
* Install requirement 
* Do aws configure --profile <profile-name>
* cd terragrunt
* Modify common-vars.yaml with the above profile
* terragrunt run-all apply


