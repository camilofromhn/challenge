resource "aws_security_group" "box" {
  name        = "${var.project}-${var.aws_region}-${var.environment}"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.vpc_id

  tags = {
    Name        = "${var.project}-${var.aws_region}-${var.environment}"
    Terraform   = "true"
    Environment = var.environment
    Scope       = var.project
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "HTTP"
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "HTTP"
  }
  

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "box" {
  ami                         = "ami-0d527b8c289b4af7f"
  instance_type               = "t2.nano"
  subnet_id                   = var.public_subnets[0]
  vpc_security_group_ids      = [aws_security_group.box.id]
  associate_public_ip_address = true
  key_name                    = var.project
  user_data                   = <<EOF
#!/bin/bash
echo "Updating apt"
sudo apt update
echo "Installing Nginx"
sudo apt install nginx -y
echo "Checking if Nginx is running" 
sudo systemctl status nginx
echo "replaceing default welcome page"
sudo echo "<html><body><h1>Hello world</h1></body></html>" | sudo tee -a /var/www/html/index.html
EOF
  tags = {
    Name        = "${var.project}-${var.aws_region}-${var.environment}"
    Terraform   = "true"
    Environment = var.environment
    Scope       = var.project
  }
}

resource "aws_eip" "box" {
  instance = aws_instance.box.id
  vpc      = true
  tags = {
    Name        = "${var.project}-${var.aws_region}-${var.environment}"
    Terraform   = "true"
    Environment = var.environment
    Scope       = var.project
  }
}
