
data "aws_availability_zones" "available" {
  state = "available"
}

locals {
  subnets = chunklist(cidrsubnets(var.cidr, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4), length(data.aws_availability_zones.available.names) )
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.project}-${var.aws_region}-${var.environment}"
  cidr = var.cidr

  azs             = data.aws_availability_zones.available.names
  private_subnets = local.subnets[0]
  public_subnets  = local.subnets[1]

  enable_dns_hostnames   = true
  enable_dns_support     = true



  tags = {
    "Terraform"                                                                       = "true"
    "Environment"                                                                     = var.environment
    "Region"                                                                          = var.aws_region
    "Project"                                                                         = var.project
  }
}
