variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
}

variable "profile" {
  description = "The AWS profile"
  type        = string
}

variable "environment" {
  description = "Environment"
  type        = string
}

variable "description" {
  description = "description"
  type        = string
}

variable "cidr" {
  description = "cidr"
  type = string
}

variable "project" {
  description = "project"
  type = string
}

