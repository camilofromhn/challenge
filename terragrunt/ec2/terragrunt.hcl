terraform {
  source = "../../modules/ec2"
}

dependency "vpc" {
  config_path = "../vpc"
}

include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region     = local.common-vars.aws_region
  version        = local.common-vars.aws_version
  profile        = local.common-vars.profile
  environment    = local.common-vars.environment
  project        = local.common-vars.project
  description    = "challenge"
  vpc_id         = dependency.vpc.outputs.vpc_id
  public_subnets = dependency.vpc.outputs.public_subnets

}
