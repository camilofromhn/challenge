terraform_version_constraint = ">= 1.1"

locals {
  common-vars = yamldecode(file("common-vars.yaml"))
}

remote_state {
  backend = "s3"
  config = {
    bucket         = "${local.common-vars.environment}-${local.common-vars.aws_region}-${local.common-vars.project}"
    key            = "${local.common-vars.environment}/${local.common-vars.aws_region}/${local.common-vars.project}/${path_relative_to_include()}/terraform.tfstate"
    region         = "${local.common-vars.aws_region}"
    encrypt        = true
    dynamodb_table = "${local.common-vars.environment}-${local.common-vars.aws_region}-${local.common-vars.project}"
    profile        = "${local.common-vars.profile}"
  }
  generate = {
    path = "backend.tf"
    if_exists = "overwrite"
  }
}