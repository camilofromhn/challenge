terraform {
  source = "../../modules/vpc"
}


include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region  = local.common-vars.aws_region
  version     = local.common-vars.aws_version
  profile     = local.common-vars.profile
  environment = local.common-vars.environment
  project     = local.common-vars.project
  description = "challenge"
  cidr        = "10.10.0.0/16"
}
